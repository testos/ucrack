package com.virjar.ucrack.hermes.weishi;

import com.google.common.collect.Maps;
import com.virjar.hermes.hermesagent.hermes_api.AsyncResult;
import com.virjar.xposed_extention.ClassLoadMonitor;

import java.util.Map;

import de.robv.android.xposed.XposedHelpers;

public class WeishiUtil {
    public static final Map<Object, AsyncResult> requestMap = Maps.newConcurrentMap();

    public static AsyncResult sendRequest(final Object requestBean) {
        XposedHelpers.callMethod(requestBean, "setRequestTime");
        Class fClass = ClassLoadMonitor.tryLoadClass("com.tencent.oscar.utils.network.wns.f");

        XposedHelpers.callMethod(XposedHelpers.callStaticMethod(fClass, "a"), "a", requestBean);
        return new AsyncResult.AsyncResultBuilder() {

            @Override
            public void bind(AsyncResult asyncResult) {
                requestMap.put(requestBean, asyncResult);
            }
        }.build();
    }
}
